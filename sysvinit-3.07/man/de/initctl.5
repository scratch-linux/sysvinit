'\" -*- coding: UTF-8 -*-
.\" Copyright (C) 2018 Jesse Smith
.\"
.\" This program is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 2 of the License.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
.\"
.\"*******************************************************************
.\"
.\" This file was generated with po4a. Translate the source file.
.\"
.\"*******************************************************************
.TH INITCTL 5 "13. April 2018" "sysvinit " Dateiformate
.SH BEZEICHNUNG
initctl \- /run/initctl ist eine benannte Pipe, die Befehle an SysV\-Init
übergibt
.SH ÜBERSICHT
/run/initctl
.SH BESCHREIBUNG

Dieses Dokument beschreibt die durch SysV \fBinit\fP unter \fI/run/initctl\fP
eingerichtete Kommunikations\-Pipe. Diese benannte Pipe erlaubt es Programmen
mit geeigneten Berechtigungen (typischerweise haben Programme, die durch
Root ausgeführt werden, Lese\- und Schreibberechtigungen auf die Pipe),
Signale an das \fBinit\fP\-Programm (PID 1) zu senden.

In der Handbuchseite von \fBinit\fP wurde bis vor kurzem noch empfohlen, dass
Benutzer, die verstehen wollen, wie Meldungen an \fBinit\fP gesendet werden,
doch den Quellcode von \fBinit\fP lesen sollten. Dies ist aber nicht wirklich
praxistauglich.

Nachrichten, die an die Pipe zur Kommunikation mit \fBinit\fP gesandt werden,
müssen einem bestimmten Format folgen. Dieses Format ist als eine C\-Struktur
definiert und die technische Detailansicht wird im Folgenden dargestellt:

/*
 *      Aufgrund veralteter Schnittstellen sind »runlevel« und »sleeptime«
 *      nicht im gleichen Struct in der Union.
 *
 *      Die komischen Größen stammen daher, dass Init erwartet, dass die
 *      gesamte Struct 384 byte groß ist.
 */

struct init_request {
        int     magic;                  /* Magische Zahl                 */
        int     cmd;                    /* Art der Anfrage               */
        int     runlevel;               /* Ziel\-Runlevel                 */
        int     sleeptime;              /* Zeit zwischen TERM und KILL   */
        union {
                struct init_request_bsd bsd;
                char                    data[368];
        } i;
};


Lassen Sie uns die init_request\-Struktur zeilenweise betrachten. Die erste
Variable (die »magische« Zahl) muss den Wert 0x03091969 haben. Das Programm
\fBinit\fP weiß dann, dass nur Programme mit Root\-Zugriff, die diese magische
Zahl senden, zur Kommunikation mit Init autorisiert sind.

Die Variable \fIcmd\fP ist (derzeit) ein Wert im Bereich 0\-8. Diese Variable
\fIcmd\fP teilt \fBinit\fP mit, was getan werden soll. Hier sind die möglichen
Optionen:

1 \- Den aktuellen Runlevel setzen, festgelegt durch die Variable »runlevel«.

2 \- Der Strom wird bald ausfallen (möglicherweise wegen eines niedrigen
Akkustandes), vorbereiten zum Herunterfahren.

3 \- Der Strom fällt aus, sofort herunterfahren.

4 \- Die Stromversorgung ist ok, Herunterfahren abbrechen.

6 \- Setzt eine Umgebungsvariable auf einen Wert, der in der Variablen
    \fIdata\fP in dieser Struktur festgelegt ist.

Andere Optionen \fIcmd\fP können später zu \fBinit\fP hinzugefügt
werden. Beispielsweise sind die Befehlswerte 0, 5 und 7 definiert, derzeit
aber nicht implementiert.

Die Variable \fIrunlevel\fP legt den Runlevel fest, zu dem gewechselt werden
soll (0\-6).

Die Variable \fIsleeptime\fP wird verwandt, wenn \fBinit\fP mitgeteilt werden
soll, die Wartezeit zwischen dem Senden von \fBSIGTERM\fP and \fBSIGKILL\fP
während des Herunterfahrprozesses zu ändern. Eine Änderung zur Laufzeit ist
noch nicht implementiert.

Die Variable \fIdata\fP (in der Union) kann zur Übergabe verschiedener Daten,
die \fBinit\fP benötigen könnte, um die Anfrage zu bearbeiten, verwandt
werden. Beispielsweise zum Setzen von Umgebungsvariablen.

Beim Setzen einer Umgebungsvariablen mittels der Pipe \fI/run/initctl\fP von
\fBinit\fP sollte die Variable \fIdata\fP das Format \fIVARIABLE\fP=\fIWERT\fP
haben. Die Zeichenkette sollte mit einem NULL\-Zeichen beendet werden.

.SH BEISPIELE

Das folgende C\-Code\-Beispiel zeigt, wie eine Umgebungsvariablen\-Anfrage
mittels der Pipe \fI/run/initctl\fP an den \fBinit\fP\-Prozess gesandt wird. Dieses
Beispiel ist vereinfacht und überspringt die Fehlerprüfung. Ein
vollständigeres Beispiel kann in der Funktion \fBinit_setnv\fP() im Programm
shutdown.c gefunden werden.

.nf
struct init_request     request;           /* oben definierte Struktur */
int                     fd;                /* Dateideskriptor für die Pipe */

memset(&request, 0, sizeof(request));      /* Struktur initialisieren */
request.magic = 0x03091969;                /* benötigte magische Zahl */
request.cmd = 6;                           /* 6 ist zum Setzen einer Variablen */
sprintf(request.data, "VARIABLE=WERT");   /* setzt VAR auf WERT in init */

if ((fd = open(INIT_FIFO, O_WRONLY)) >= 0) /* Pipe zum Schreiben öffnen */
{
    size_t s  = sizeof(request);           /* Größe der zu schreibenden Struktur */
    void *ptr = &request;                  /* temporärer Zeiger */
    write(fd, ptr, s);                     /* Struktur an die Pipe schicken */
    close(fd);                             /* Pipe schließen, wenn fertig */
}
.fi

.sp
.SH ANMERKUNGEN
Normalerweise würde die Pipe \fI/run/initctl\fP nur von systemnahen Programmen
zur Anfrage von durch die Stromversorgung ausgelöstem Herunterfahren oder
Änderungen des Runlevels verwandt, wie bei \fBtelinit\fP. Meistens ist es nicht
notwendig, direkt mit \fBinit\fP zu kommunizieren. Dies ermöglicht aber einen
erweiterbaren Ansatz, so dass \fBinit\fP beigebracht werden kann, weitere
Befehle zu lernen.
.PP
Die über die Pipe \fI/run/initctl\fP gesandten Befehle müssen in einem
bestimmten Binärformat und mit einer bestimmten Länge gesandt
werden. Größere Datenstrukturen oder solche, die nicht das korrekte Format
verwenden, werden ignoriert. Typischerweise hat aus Sicherheitsgründen nur
der Systemverwalter die Möglichkeit, in die initctl\-Pipe zu schreiben.
.PP
Die Pipe \fI/run/initctl\fP kann durch Senden des Signals \fBSIGUSR2\fP an \fBinit\fP
(PID 1) geschlossen werden. Damit wird die Pipe geschlossen und verbleibt
geschlossen. Das ist nützlich, um sicherzustellen, dass \fBinit\fP keine
Dateien offenbehält. Wenn die Pipe allerdings geschlossen ist, dann empfängt
\fBinit\fP keine Signale mehr, wie die von \fBshutdown\fP(8) oder \fBtelinit\fP(8)
gesandten. Mit anderen Worten: Wird die Pipe geschlossen, kann \fBinit\fP
seinen Runlevel nicht mehr direkt ändern. Die Pipe kann wieder geöffnet
werden, indem \fBinit\fP (PID 1) das Signal \fBSIGUSR1\fP gesandt wird.
.PP
Falls die Pipe \fI/run/initctl\fP geschlossen ist, könnte das System immer noch
über den Schalter \fB\-n\fP von \fBshutdown\fP(8) heruntergefahren werden; dies ist
aber nicht sauber und wird nicht empfohlen.

.SH DATEIEN
/run/initctl /sbin/init

.SH AUTOR
.MT jsmith@\:resonatingmedia\:.com
Jesse Smith
.ME
.SH "SIEHE AUCH"
\fBinit\fP(8)
